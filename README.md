# FrontEnd Development Bootcamp - HTML and CSS - Project 1

## 1. Basics
This project was meant to cover all necessary basic elements which are needed to construct a modern website. 
Begining from the HTML structure, through basic and advanced styling, ending with creating GIT code repository.

## 2. What I have learnt during the project?
I have learnt a lot of about buliding these days websites. How to construct and style menu, including:
* Fluid technics in creating flexible websites
* Flexbox and Grid
* Bootstrap framework
* Media queries and and good RWD practicies
* Fundamentals of Git system using GitLAb as an example

